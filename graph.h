#ifndef GRAPH_H
#define	GRAPH_H
#ifdef	__cplusplus
extern "C" {
#endif
struct graph;
struct graph{
    struct graph **proches; // vector
    char *label;
    unsigned int color : 1;
    size_t proches_size;
};

typedef struct graph graph_t;



#ifdef	__cplusplus
}
#endif

#endif	/* GRAPH_H */

