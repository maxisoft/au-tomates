#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "bool.h"
#include "graph.h"
#define ETAT_PUIT -3
#define FUSION_STRING "-"

const char LETTERS[] = "abcdefghijklmnopqrstuvwxyz";

typedef struct s_liste {
    int state;
    struct s_liste* suiv;

} liste;

typedef struct {
    /**
     * le champ size donne le nombre dâ€™Â´etats de lâ€™automate. Par convention, les Â´etats seront obligatoire-ment numÂ´erotÂ´es de 0 `a size - 1.
     */
    unsigned int size;
    /**
     * le champ sizealpha donne la taille de lâ€™alphabet. Par convention encore, on consid`erera que les
     * lettres sont les sizealpha premi`eres lettres de lâ€™alphabet en minuscule. Par exemple si sizealpha
     *  vaut 3, lâ€™alphabet sera composÂ´e des lettres a, b et c.
     */
    unsigned int sizealpha : 5;
    /**
     * le champ initial est un tableau dâ€™entiers de taille size qui contient `a la i-`eme case la valeur 1 si
     * lâ€™Â´etat i est initial, 0 sinon.
     */
    bool* initial;
    /**
     * le champ final est un tableau dâ€™entiers de taille size qui contient `a la i-`eme case la valeur 1 si
     * lâ€™Â´etat i est ï¬nal, 0 sinon.
     */
    bool* final;
    
    /**
     * Liste des transitions
     */
    liste*** trans;
    
    unsigned int error : 1;
    
    /**
     * listes des labels des etats
     */
    char **labels;
} automate;

/**
 * Ajoute a une liste triée et unique q
 * @param l
 * @param q
 * @return 
 */
bool ajouteListe(liste** l, int q) {
    liste* ptl;
    liste* tmp;
    ptl = *l;
    if (!ptl) {
        ptl = (liste*) malloc(sizeof (liste));
        ptl->state = q;
        ptl->suiv = NULL;
        *l = ptl;
        return true;
    }
    if (ptl->state == q) {
        return false;
    }
    if (q < ptl->state) {
        tmp = *l;
        *l = (liste*) malloc(sizeof (liste));
        (*l)->state = q;
        (*l)->suiv = tmp;
        return true;
    }
    while (ptl->suiv && ptl->suiv->state < q) {
        ptl = ptl->suiv;
    }
    if (!ptl->suiv) {
        ptl->suiv = (liste*) malloc(sizeof (liste));
        ptl = ptl->suiv;
        ptl->state = q;
        ptl->suiv = NULL;
        return true;
    }
    if (ptl->suiv->state == q) {
        return false;
    }
    tmp = ptl->suiv;
    ptl->suiv = (liste*) malloc(sizeof (liste));
    ptl = ptl->suiv;
    ptl->state = q;
    ptl->suiv = tmp;
    return true;
}

/**
 * Retourne l'etats correspondant au labels
 * @param a l'automate
 * @param s le label
 * @return -1 si erreur; l'index de l'etats sinon
 */
int getListIndexFromStr(automate a, char* s) {
    char* curr;
    for (int i = 0; i < a.size; ++i) {
        curr = a.labels[i];
        if (strcmp(curr, s) == 0) {
            return i;
        }
    }
    return -1;
}

/**
 * Conversion d'une lettre en entier
 * @param c
 * @return -1 si erreur.
 */
int letterToInt(char c) {
    if (c > 'z' || c < 'a') {
        return -1;
    }
    return c - 'a';
}

/**
 * Ajoute une transition a un automate.
 * version ou on passe les int
 * @param a
 * @param from
 * @param to
 * @param c
 * @return true si bien ajouter
 */
bool _ajouteTransition(automate a, int from, int to, char c) {
    c = (char) letterToInt(c);
    if (c < 0 || from >= a.size || c >= a.sizealpha) {
        return false;
    }
    return ajouteListe(&(a.trans[from][c]), to);
}

/**
 * Ajoute une transition a un automate.
 * version ou on passe des string.
 * @param a
 * @param fromstr
 * @param tostr
 * @param c
 * @return 
 */
bool ajouteTransition(automate a, char *fromstr, char *tostr, char c) {
    int from = getListIndexFromStr(a, fromstr);
    int to = getListIndexFromStr(a, tostr);
    return from >= 0 && to >= 0 && _ajouteTransition(a, from, to, c);
}

/**
 * Construit un automate d'exemple
 * @return l'automate
 */
automate construitAutomateExemple() {
    automate ret = {.size = 5, .sizealpha = 2, .error = false};
    ret.initial = calloc(ret.size, sizeof (bool));
    ret.initial[0] = true;
    ret.initial[1] = true;

    ret.final = calloc(ret.size, sizeof (bool));
    ret.final[1] = true;
    ret.final[4] = true;

    ret.trans = malloc(ret.size * sizeof (liste**));

    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }

    ret.labels = malloc(ret.size * sizeof (liste**));
    for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
        ret.labels[i] = calloc(calloc_len, sizeof (char));
        sprintf(ret.labels[i], "%i", i);
    }


    ret.error |= !ajouteTransition(ret, "0", "1", 'a');
    ret.error |= !ajouteTransition(ret, "0", "2", 'a');
    ret.error |= !ajouteTransition(ret, "0", "3", 'a');

    ret.error |= !ajouteTransition(ret, "1", "3", 'b');

    ret.error |= !ajouteTransition(ret, "2", "3", 'a');
    ret.error |= !ajouteTransition(ret, "2", "4", 'b');

    ret.error |= !ajouteTransition(ret, "3", "3", 'b');
    ret.error |= !ajouteTransition(ret, "3", "4", 'b');

    ret.error |= !ajouteTransition(ret, "4", "4", 'a');
    return ret;
}

/**
 * affiche sur stdout un automate.
 * @param a
 */
void afficheAutomate(automate a) {
    int state, letter, currstate;
    liste *curr;
    printf("Les etats initaux\n");
    for (int i = 0; i < a.size; ++i) {
        if (a.initial[i]) printf("%s ", a.labels[i]);
    }
    printf("\nLes etats finaux\n");
    for (int i = 0; i < a.size; ++i) {
        if (a.final[i]) printf("%s ", a.labels[i]);
    }
    printf("\nLes Transitions\n");
    for (state = 0; state < a.size; ++state) {
        printf("----------------------------------\nDepuis l'etat %s\n", a.labels[state]);
        for (letter = 0; letter < a.sizealpha; ++letter) {
            printf("avec la lettre %c :\n", LETTERS[letter]);
            if (curr = a.trans[state][letter]) {
                do {
                    currstate = curr->state;
                    printf("%s ", currstate == ETAT_PUIT ? "PUIT" : a.labels[currstate]);
                } while (curr = curr->suiv);
            }
            printf("\n");
        }
    }
}

/**
 * retourne le nombre de transitions d'un automate
 * @param a l'automate
 * @return 
 */
unsigned int compteTransitions(automate a) {
    int letter;
    unsigned int ret = 0;
    liste *curr;
    for (int state = 0; state < a.size; ++state) {
        for (letter = 0; letter < a.sizealpha; ++letter) {
            if (curr = a.trans[state][letter]) {
                do {
                    ret += 1;
                } while (curr = curr->suiv);
            }
        }
    }
    return ret;
}

/**
 * Un automate fini dÃ©terministe sur un alphabet A est un automate fini qui vÃ©rifie les deux conditions suivantes :
 *  - il possÃ¨de un seul Ã©tat initial ;
 *  - pour tout Ã©tat q, et pour toute lettre a, il existe au plus une transition partant de q et portant l'Ã©tiquette a.
 * @param a
 * @return 
 */
bool deterministe(automate a) {
    int letter;
    int cnt = 0;

    /* un seul Ã©tat initial */
    for (int i = 0; i < a.size && cnt < 2; ++i) {
        cnt += a.initial[i];
    }
    if (cnt != 1) {
        return false;
    }

    /* au plus une transition partant de `state` et portant l'Ã©tiquette `letter` */
    for (int state = 0; state < a.size; ++state) {
        for (letter = 0; letter < a.sizealpha; ++letter) {
            if (a.trans[state][letter] && a.trans[state][letter]->suiv) { // => deux transition
                return false;
            }
        }
    }
    return true;
}

/**
 * Un automate est complet si pour tout Ã©tat q, et pour toute lettre a, il existe au moins une transition partant de q et portant l'Ã©tiquette a.
 * @param a
 * @return 
 */
bool complet(automate a) {
    int letter;
    /* au moins une transition partant de `state` et portant l'Ã©tiquette `letter` */
    for (int state = 0; state < a.size; ++state) {
        for (letter = 0; letter < a.sizealpha; ++letter) {
            if (!a.trans[state][letter]) { // => 0 transition
                return false;
            }
        }
    }
    return true;
}

/**
 * Supprime transition.
 * version avec int
 * @param a
 * @param from
 * @param to
 * @param c
 * @return 
 */
bool _supprimeTransition(automate a, int from, int to, char c) {
    c = (char) letterToInt(c);
    if (c < 0 || from >= a.size || c >= a.sizealpha) {
        return false;
    }

    liste *curr, *prev, *next;

    curr = a.trans[from][c];
    prev = NULL;
    if (curr == NULL) {
        return false;
    }

    do {
        if (curr->state == to) {
            //on refait les liens et on free
            next = curr->suiv;

            if (prev == NULL) { // debut de la liste
                a.trans[from][c] = next;
            } else {
                prev->suiv = next;
            }
            free(curr);
            return true;
        }
        prev = curr;
    } while (curr = curr->suiv);
    return false;
}

/**
 * Supprime transition.
 * version avec string
 * @param a
 * @param from
 * @param to
 * @param c
 * @return 
 */
bool supprimeTransition(automate a, char *fromstr, char *tostr, char c) {
    int from = getListIndexFromStr(a, fromstr);
    int to = getListIndexFromStr(a, tostr);
    return from >= 0 && to >= 0 && _supprimeTransition(a, from, to, c);
}

/**
 * Supprime l'etat d'un automate.
 * version avec int
 * @param ap
 * @param from
 * @return 
 */
bool _supprimeEtat(automate* ap, int from) {
    liste *curr, *next, *prev;

    if (from >= ap->size) {
        return false;
    }


    for (int trans = 0; trans < ap->sizealpha; ++trans) { // pour chaque transition de l'etat a supprimer.

        curr = ap->trans[from][trans];
        if (curr == NULL) {
            continue;
        }
        do {
            //on free
            next = curr->suiv;
            free(curr);
            curr = next;
        } while (curr);

        ap->trans[from][trans] = NULL;
    }

    prev = NULL;

    for (int state = 0; state < ap->size; ++state) { // Pour tous les etats sauf celui ci dessus
        if (state == from) {
            continue;
        }
        for (int trans = 0; trans < ap->sizealpha; ++trans) { // pour toutes les transitions de chaque etats.
            _supprimeTransition(*ap, state, from, trans + 'a'); //essaye du supprimer.
        }
    }
    //suppression du label
    free(ap->labels[from]);

    ap->size -= 1;
    //decalage
    for (int i = from; i < ap->size; ++i) {
        ap->initial[i] = ap->initial[i + 1];
        ap->final[i] = ap->final[i + 1];
        ap->trans[i] = ap->trans[i + 1];
        ap->labels[i] = ap->labels[i + 1];
    }

    //decalage dans toutes les liste restante.
    for (int i = 0; i < ap->size; ++i) {
        for (int j = 0; j < ap->sizealpha; ++j) {
            curr = ap->trans[i][j];
            while (curr) {
                if (curr->state > from) {
                    curr->state -= 1;
                }
                curr = curr->suiv;
            }
        }
    }

    //realloc les tableau
    ap->initial = realloc(ap->initial, ap->size * sizeof (bool));
    ap->final = realloc(ap->final, ap->size * sizeof (bool));
    ap->trans = realloc(ap->trans, ap->size * sizeof (liste**));
    ap->labels = realloc(ap->labels, ap->size * sizeof (char*));
    return true;
}
/**
 * Supprime un Etat d'un automate.
 * version avec string.
 * @param ap
 * @param fromstr
 * @return 
 */
bool supprimeEtat(automate* ap, char *fromstr) {
    int from = getListIndexFromStr(*ap, fromstr);
    return from >= 0 && _supprimeEtat(ap, from);
}

/**
 * Complete un automate.
 * @param a l'automate a completer
 */
void completeAutomate(automate a) {
    liste *curr;
    for (int state = 0; state < a.size; ++state) {
        for (int letter = 0; letter < a.sizealpha; ++letter) {
            curr = a.trans[state][letter];
            if (!curr) {
                _ajouteTransition(a, state, ETAT_PUIT, letter + 'a');
            }
        }
    }
}

/**
 * Suppression d'un automate.
 * @param a l'automate
 */
void delAutomate(automate a){
    for (int i = 0; i<a.size; ++i){
        for (int j; j< a.sizealpha; ++j){
            liste * curr = a.trans[i][j];
            while(curr){
                liste *tmp = curr->suiv;
                free(curr);
                curr = tmp;
            }
        }
        free(a.trans[i]);
        free(a.labels[i]);
    }
    free(a.trans);
    free(a.labels);
    free(a.initial);
    free(a.final);
}

/**
 * fusionne les etat d'un automate.
 * @param ap pointeur sur l'automate
 * @param state1str
 * @param state2str
 * @return 
 */
bool fusionEtats(automate *ap, char *state1str, char *state2str) {
    int state1 = getListIndexFromStr(*ap, state1str);
    int state2 = getListIndexFromStr(*ap, state2str);

    if (ap->initial[state2]) {
        ap->initial[state1] = true;
    }
    if (ap->final[state2]) {
        ap->final[state1] = true;
    }

    // pour tous les transition a partir de l'etat 2
    for (int alpha = 0; alpha < ap->sizealpha; ++alpha) {
        liste *l = ap->trans[state2][alpha];
        while (l) {
            if (l->state == state2) {
                _ajouteTransition(*ap, state1, state1, alpha + 'a');
            } else {
                _ajouteTransition(*ap, state1, l->state, alpha + 'a');
            }
            l = l->suiv;
        }
    }

    //pour toutes les autres transitions qui arrive a l'etat 2
    for (int state = 0; state < ap->size; ++state) {
        if (state == state2) {
            continue;
        }
        for (int alpha = 0; alpha < ap->sizealpha; ++alpha) {
            liste *l = ap->trans[state][alpha];
            while (l) {
                if (l->state == state2) {
                    _ajouteTransition(*ap, state, state1, alpha + 'a');
                }
                l = l->suiv;
            }
        }
    }

    ap->labels[state1] = realloc(ap->labels[state1], (strlen(state1str) + strlen(state2str) + strlen(FUSION_STRING) + 1) * sizeof (char));
    sprintf(ap->labels[state1], "%s%s%s", state1str, FUSION_STRING, state2str);
    _supprimeEtat(ap, state2);
}

//Test du vide

/**
 * fonction recursive pour trouver un chemin dans un graph
 * @param graph
 * @param q
 * @return 
 */
static bool _rec_chemin(graph_t* graph, graph_t *q) {
    if (graph->color) {
        return false;
    }

    graph->color += 1;
    if (graph == q) {
        return true;
    }
    bool ret = false;
    for (int i = 0; i < graph->proches_size && !ret; ++i) {
        ret = _rec_chemin(graph->proches[i], q);
    }
    return ret;
}
/**
 * recherche d'un chemin dans un graph
 * @param graphs
 * @param len
 * @param pstr
 * @param qstr
 * @return 
 */
bool chemin(graph_t** graphs, size_t len, char *pstr, char* qstr) {
    //1st of all find p an q
    graph_t *p = NULL;
    graph_t *q = NULL;
    for (int i = 0; i < len && (!p || !q); ++i) {
        graph_t * curr = graphs[i];
        if (strcmp(curr->label, pstr) == 0) {
            p = curr;
        }
        if (strcmp(curr->label, qstr) == 0) {
            q = curr;
        }
    }
    if (!p || !q) {
        return false;
    }

    //parcours en profondeur.
    return _rec_chemin(p, q);
}

/**
 * Conversion d'un automates en graph
 * @param a l'automate
 * @return liste des graphs crée.
 */
graph_t** automateToGraphe(automate a) {
    graph_t **cachegraph = calloc(a.size, sizeof (graph_t*));
    for (int i = 0; i < a.size; ++i) {
        graph_t *currgraph = calloc(1, sizeof (graph_t));
        /*currgraph->color = 0;
        currgraph->proches = NULL;
        currgraph->size = 0;*/
        currgraph->label = a.labels[i];
        cachegraph[i] = currgraph;
    }

    for (int i = 0; i < a.size; ++i) {
        graph_t *currgraph = cachegraph[i];
        for (int alpha = 0; alpha < a.sizealpha; ++alpha) {
            liste *l = a.trans[i][alpha];
            while (l) {
                bool cont = false;
                for (int j = 0; currgraph->proches && j < currgraph->proches_size; ++j) {
                    if (currgraph->proches[i] == cachegraph[l->state]) {
                        cont = true;
                        break;
                    }
                }
                if (cont) {
                    continue;
                }
                if (!currgraph->proches) {
                    currgraph->proches = calloc(1, sizeof (graph_t*));
                } else {
                    currgraph->proches = realloc(currgraph->proches, currgraph->proches_size + 1);
                }
                currgraph->proches[currgraph->proches_size] = cachegraph[l->state];
                currgraph->proches_size += 1;
                l = l->suiv;
            }
        }
    }
    return cachegraph;
}

/**
 *  retourne 1 si le langage reconnu par un automate est vide, 0 sinon.
 * @return 
 */
bool langageVide(automate a) {
    bool cont = true;
    graph_t **graphs = automateToGraphe(a);
    for (int i = 0; i < a.size && cont; ++i) {
        if (a.initial[i]) {
            for(int j=0 ; j< a.size && cont; j++){
                if(a.final[j]){
                    cont = !chemin(graphs, a.size, a.labels[i], a.labels[j]);
                }
            }
        }
    }
    for (int i = 0; i< a.size && cont; ++i ){
        free(graphs[i]->proches);
        free(graphs[i]);
    }
    free(graphs);
    return cont;
}

void supprimeNonCoAccessibles(automate *ap) {
    //TODO
}

//Supprime d’un automate les etats qui ne sont pas accessibles.

void supprimeNonAccessibles(automate *ap) {
    graph_t** graphs = automateToGraphe(*ap);

    bool *accessible = calloc(ap->size, sizeof (bool));
    for (int i = 0; i < ap->size && !accessible[i]; ++i) {
        graph_t* currgraphs = graphs[i];
        if (ap->initial[getListIndexFromStr(*ap, currgraphs->label)]) {
            continue;
        }
        for (int j; j < ap->size && !accessible[i]; ++j) {
            accessible[i] = _rec_chemin(graphs[j], currgraphs);
        }
    }
    bool found = false;
    for (int i = 0; i < ap->size; ++i) {
        if (!accessible[i]) {
            supprimeEtat(ap, graphs[i]->label);
            found = true;
        }
        free(graphs[i]->proches);
        free(graphs[i]);
    }
    free(graphs);
    free(accessible);
    if (found) {
        supprimeNonAccessibles(ap); // recursivitée jusqu'a stabilisation.
    }
}

automate construction_automate_7_5_1() {
    automate ret = {.size = 2, .sizealpha = 2, .error = false};
    ret.initial = calloc(ret.size, sizeof (bool));
    ret.initial[0] = true;

    ret.final = calloc(ret.size, sizeof (bool));
    ret.final[1] = true;

    ret.trans = malloc(ret.size * sizeof (liste**));

    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }

    ret.labels = malloc(ret.size * sizeof (liste**));
    for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
        ret.labels[i] = calloc(calloc_len, sizeof (char));
        sprintf(ret.labels[i], "%i", i);
    }


    ret.error |= !ajouteTransition(ret, "0", "0", 'a');
    ret.error |= !ajouteTransition(ret, "0", "1", 'b');
    ret.error |= !ajouteTransition(ret, "1", "1", 'a');
    return ret;
}

automate construction_automate_7_5_2() {
    automate ret = {.size = 3, .sizealpha = 2, .error = false};
    ret.initial = calloc(ret.size, sizeof (bool));
    ret.initial[0] = true;

    ret.final = calloc(ret.size, sizeof (bool));
    ret.final[2] = true;

    ret.trans = malloc(ret.size * sizeof (liste**));

    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }

    ret.labels = malloc(ret.size * sizeof (liste**));
    for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
        ret.labels[i] = calloc(calloc_len, sizeof (char));
        sprintf(ret.labels[i], "%i", i);
    }


    ret.error |= !ajouteTransition(ret, "0", "0", 'b');
    ret.error |= !ajouteTransition(ret, "0", "1", 'a');

    ret.error |= !ajouteTransition(ret, "1", "1", 'b');
    ret.error |= !ajouteTransition(ret, "1", "2", 'b');

    ret.error |= !ajouteTransition(ret, "2", "2", 'b');
    return ret;
}

//Produit d’automates

automate produit(automate a1, automate a2) {
    size_t ret_size = a1.size * a2.size;
    size_t min_size = a1.size < a2.size ? a1.size : a2.size;

    automate ret = {.size = ret_size, .sizealpha = a1.sizealpha, .error = false};
    if (a1.sizealpha != a2.sizealpha) {
        ret.error = true;
        return ret;
    }
    ret.trans = malloc(ret.size * sizeof (liste**));
    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }

    ret.labels = malloc(ret_size * sizeof (char *));
    for (int i = 0; i < a1.size; ++i) {
        char *curra1label = a1.labels[i];
        for (int j = 0; j < a2.size; ++j) {
            //labels
            char *curra2label = a2.labels[j];
            int index = i * a2.size + j;
            ret.labels[index] = calloc(strlen(curra1label) + strlen(curra2label) + strlen(",") + 1, sizeof (char));
            sprintf(ret.labels[index], "%s,%s", curra1label, curra2label);
        }
    }

    //transition
    liste *a1trans;
    liste *a2trans;
    for (int i = 0; i < ret.size; ++i) {
        int p = i / a2.size;
        int q = i % a2.size;
        for (int alpha = 0; alpha < ret.sizealpha; ++alpha) {
            a1trans = a1.trans[p][alpha];
            while (a1trans) {
                if (a1trans->state == p) {
                    a1trans = a1trans->suiv;
                    continue;
                }
                a2trans = a2.trans[q][alpha];
                while (a2trans) {
                    _ajouteTransition(ret, i, a1trans->state * a2.size + a2trans->state, alpha + 'a');
                    a2trans = a2trans->suiv;
                }
                a1trans = a1trans->suiv;
            }

            a2trans = a2.trans[q][alpha];
            while (a2trans) {
                if (a2trans->state == q) {
                    a2trans = a2trans->suiv;
                    continue;
                }
                a1trans = a1.trans[p][alpha];
                while (a1trans) {
                    _ajouteTransition(ret, i, a1trans->state * a2.size + a2trans->state, alpha + 'a');
                    a1trans = a1trans->suiv;
                }
                a2trans = a2trans->suiv;
            }
        }
    }


    ret.initial = calloc(ret_size, sizeof (bool));
    ret.final = calloc(ret_size, sizeof (bool));
    for (int i = 0; i < a1.size; ++i) {
        for (int j = 0; j < a2.size; ++j) {
            if (a1.initial[i] && a2.initial[j]) {
                int index = i * a2.size + j;
                ret.initial[index] = true;
            }
            if (a1.final[i] && a2.final[j]) {
                int index = i * a2.size + j;
                ret.final[index] = true;
            }
        }

    }
    return ret;
}

bool intersectionVide(automate a1, automate a2) {
    automate tmp = produit(a1, a2);
    bool ret = langageVide(tmp);
    delAutomate(tmp);
    return ret;
}

struct tree {
    int value;
    struct tree **childs;
    size_t childsLen;
    unsigned int final : 1;
    unsigned int root : 1;
};

typedef struct tree tree_t;

int binary_search(int A[], int key, int imin, int imax) {
    while (imax >= imin) {
        int imid = imin + ((imax - imin) / 2);
        if (A[imid] == key)
            return imid;
        else if (A[imid] < key)
            imin = imid + 1;
        else
            imax = imid - 1;
    }
    return -1;
}

int tree_binary_search(tree_t *A, int key, int imin, int imax) {
    while (imax >= imin) {
        int imid = imin + ((imax - imin) / 2);
        if (A->childs[imid]->value == key)
            return imid;
        else if (A->childs[imid]->value < key)
            imin = imid + 1;
        else
            imax = imid - 1;
    }
    return -1;
}

int tree_cpr(const void * a, const void * b) {
    return ( (*(tree_t**) a)->value - (*(tree_t**) b)->value);
}

void tree_add_child(tree_t *self, tree_t *child) {
    if (!self->childs) {
        self->childs = calloc(1, sizeof (tree_t*));
        self->childs[self->childsLen] = child;
        self->childsLen += 1;
    }
    int search_result = tree_binary_search(self, child->value, 0, self->childsLen - 1);
    if (search_result == -1) {
        self->childs = realloc(self->childs, (self->childsLen + 1) * sizeof (tree_t*));
        self->childs[self->childsLen] = child;
        self->childsLen += 1;
        qsort(self->childs, self->childsLen, sizeof (tree_t*), tree_cpr);
    } else if (child->childs) {
        for (int i = 0; i < child->childsLen; ++i) {
            tree_add_child(self->childs[search_result], child->childs[i]);
        }
    } else {
        self->childs[search_result]->final = true;
    }
}

tree_t * tree_create_root(bool final) {
    tree_t *ret = calloc(1, sizeof (tree_t));
    ret->root = true;
    ret->final = final;
    return ret;
}

tree_t * tree_create(int value, bool final) {
    tree_t *ret = calloc(1, sizeof (tree_t));
    ret->value = value;
    ret->final = final;
    return ret;
}

void tree_add_from(tree_t *self, int *array, size_t len) {
    tree_t *currtree = NULL;
    bool final = true;
    for (int i = len - 1; i >= 0; --i) {
        int curr = array[i];
        tree_t *tmp = tree_create(curr, final);
        if (currtree) {
            tree_add_child(tmp, currtree);
        }
        currtree = tmp;
        final = false;
    }
    tree_add_child(self, currtree);
}

bool tree_est_reconus(tree_t *self, int *array, size_t len) {
    //if (len == -1)
    int search_result = tree_binary_search(self, array[0], 0, self->childsLen - 1);
    if (search_result == -1) {
        return false;
    }
    if (len == 1) {
        return self->childs[search_result]->final;
    }
    return tree_est_reconus(self->childs[search_result], array + 1, len - 1);
}

int compare_int(const void * a, const void * b) {
    return ( *(int*) a - *(int*) b);
}

void tree_deep_delete(tree_t *self) {
    for (int i = 0; i < self->childsLen; ++i) {
        tree_deep_delete(self->childs[i]);
    }
    for (int i = 0; i < self->childsLen; ++i) {
        free(self->childs[i]);
    }
    free(self->childs);
    //free(self);
}

automate construction_automate_7_6() {
    automate ret = {.size = 3, .sizealpha = 2, .error = false};
    ret.initial = calloc(ret.size, sizeof (bool));
    ret.initial[0] = true;

    ret.final = calloc(ret.size, sizeof (bool));
    ret.final[2] = true;

    ret.trans = malloc(ret.size * sizeof (liste**));

    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }

    ret.labels = malloc(ret.size * sizeof (liste**));
    for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
        ret.labels[i] = calloc(calloc_len, sizeof (char));
        sprintf(ret.labels[i], "%i", i);
    }


    ret.error |= !ajouteTransition(ret, "0", "0", 'b');
    ret.error |= !ajouteTransition(ret, "0", "0", 'a');
    ret.error |= !ajouteTransition(ret, "0", "1", 'a');

    ret.error |= !ajouteTransition(ret, "1", "2", 'a');
    ret.error |= !ajouteTransition(ret, "1", "2", 'b');
    return ret;
}

char *set_to_str(int *set, size_t len) {
    size_t calloclen = 2;
    int tmp = strlen(", ");
    for (int i = 0; i < len; ++i) {
        calloclen += floor(log10(abs(set[i]))) + 1 + tmp;
    }
    char *ret = calloc(calloclen + 1, sizeof (char));
    ret[0] = '{';
    for (int i = 0; i < len - 1; ++i) {
        sprintf(ret + strlen(ret), "%i, ", set[i]);
    }
    sprintf(ret + strlen(ret), "%i}", set[len - 1]);
    return ret;
}

char *set_to_str_with_automate(int *set, size_t len, automate a) {
    size_t calloclen = 2;
    int tmp = strlen(", ");
    for (int i = 0; i < len; ++i) {
        calloclen += strlen(a.labels[set[i]]) + tmp;
    }
    char *ret = calloc(calloclen + 1, sizeof (char));
    ret[0] = '{';
    for (int i = 0; i < len - 1; ++i) {
        sprintf(ret + strlen(ret), "%s, ", a.labels[set[i]]);
    }
    sprintf(ret + strlen(ret), "%s}", a.labels[set[len - 1]]);
    return ret;
}

automate determinise(automate a) {
    tree_t *deja_vu = tree_create_root(false);
    automate ret = {.size = a.size, .sizealpha = a.sizealpha, .error = false};
    ret.initial = calloc(ret.size, sizeof (bool));
    ret.final = calloc(ret.size, sizeof (bool));

    ret.trans = malloc(ret.size * sizeof (liste**));
    for (int i = 0; i < ret.size; ++i) {
        ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
    }
    ret.labels = calloc(ret.size, sizeof (char*));
    size_t ret_index = 0;
    size_t ret_maxsize = a.size;

    int * set = calloc(ret.size, sizeof (int));
    size_t setsize = 0;

    int **queue_set = calloc(ret.size + 1, sizeof (int*)); // LIFO
    size_t *queue_set_itemssize = calloc(ret.size + 1, sizeof (size_t)); //la taille de chaque set contenue dans queue_set
    size_t queue_setsize = 0;
    size_t queue_maxsize = ret.size + 1; // la taille maximum des deux vector ci-dessus 

    //initialisation avec les etats initiaux
    for (int i = 0; i < a.size; ++i) {
        if (a.initial[i]) {
            set[setsize++] = i;
        }
    }
    //set est deja trier
    
    ret.size = 0; //vrai taille
    
    // initialisation de la queue
    queue_set[queue_setsize] = set; 
    queue_set_itemssize[queue_setsize] = setsize;
    queue_setsize += 1;

    char *set_str = set_to_str_with_automate(set, setsize, a);
    if (getListIndexFromStr(ret, set_str) == -1) { // => MISS
        ret.labels[ret_index] = set_str;
        for (int i = 0; i < setsize; ++i) {
            int curr = set[i];
            if (!ret.initial[ret_index] && a.initial[curr]) {
                ret.initial[ret_index] = true;
            }
            if (!ret.final[ret_index] && a.final[curr]) {
                ret.final[ret_index] = true;
            }
        }
        ret.size += 1;
        if (ret.size >= ret_maxsize) {
            ret_maxsize *= 2;
            ret.initial = realloc(ret.initial, ret_maxsize * sizeof (bool));
            ret.final = realloc(ret.final, ret_maxsize * sizeof (bool));
            ret.labels = realloc(ret.labels, ret_maxsize * sizeof (char*));
            ret.trans = realloc(ret.trans, ret_maxsize * sizeof (liste**));
            for (int j = ret.size; j < ret_maxsize; ++j) {
                ret.initial[j] = false;
                ret.final[j] = false;
                ret.labels[j] = NULL;
                ret.trans[j] = calloc(ret.sizealpha, sizeof (liste*));
            }
        }
        ret_index += 1;
    } else {
        free(set_str);
    }
    //fin initialisation.
    while (queue_setsize) {
        queue_setsize -= 1;
        set = queue_set[queue_setsize];
        setsize = queue_set_itemssize[queue_setsize];

        for (int i = 0; i < ret.sizealpha; ++i) { // pour toutes les trans
            int *addset = calloc(a.size, sizeof (int));
            size_t addsetsize = 0;
            size_t addsetmaxsize = a.size;
            for (int j = 0; j < setsize; ++j) {// pour tous les items du set
                liste *l = a.trans[set[j]][i];
                while (l) {
                    if (binary_search(addset, l->state, 0, addsetsize - 1) != -1) { // => deja dans le set
                        l = l->suiv;
                        continue;
                    }
                    addset[addsetsize] = l->state;
                    addsetsize += 1;
                    if (addsetsize >= addsetmaxsize) {
                        addsetmaxsize *= 2;
                        addset = realloc(addset, addsetmaxsize * sizeof (int));
                    }
                    l = l->suiv;
                }
                qsort(addset, addsetsize, sizeof (int), compare_int);
            }
            if (!addsetsize){ // aucun ajout
                continue;
            }
            if (tree_est_reconus(deja_vu, addset, addsetsize)) { // deja reconnus mais peut être de nouvelle transition.
                //on crée les transition
                char *src = set_to_str_with_automate(set, setsize, a);
                char *dest = set_to_str_with_automate(addset, addsetsize, a);
                ajouteTransition(ret, src, dest, i + 'a');
                free(src);
                free(dest);
                free(addset);
                continue;
            }
            set_str = set_to_str_with_automate(addset, addsetsize, a);
            int index = getListIndexFromStr(ret, set_str);
            if (index == -1) { // => MISS
                ret.labels[ret_index] = set_str;
                for (int i = 0; i < addsetsize; ++i) {
                    int curr = addset[i];
                    if (!ret.final[ret_index] && a.final[curr]) {
                        ret.final[ret_index] = true;
                    }
                }
                ret.size += 1;
                if (ret.size >= ret_maxsize) {
                    ret_maxsize *= 2;
                    ret.initial = realloc(ret.initial, ret_maxsize * sizeof (bool));
                    ret.final = realloc(ret.final, ret_maxsize * sizeof (bool));
                    ret.labels = realloc(ret.labels, ret_maxsize * sizeof (char*));
                    ret.trans = realloc(ret.trans, ret_maxsize * sizeof (liste**));
                    for (int j = ret.size; j < ret_maxsize; ++j) {
                        ret.initial[j] = false;
                        ret.final[j] = false;
                        ret.labels[j] = NULL;
                        ret.trans[j] = calloc(ret.sizealpha, sizeof (liste*));
                    }
                }
                index = ret_index;
                ret_index += 1;
            } else {
                free(set_str);
            }

            //on crée les transition
            char *src = set_to_str_with_automate(set, setsize, a);
            char *dest = set_to_str_with_automate(addset, addsetsize, a); // note : on peut reprendre la valeur de set_str mais ca fait un code encore moins lisible.
            ajouteTransition(ret, src, dest, i + 'a');
            free(src);
            free(dest);

            queue_set[queue_setsize] = addset; // ajout dans la queue.

            queue_set_itemssize[queue_setsize] = addsetsize;
            queue_setsize += 1;

            if (queue_setsize >= queue_maxsize) {
                queue_maxsize *= 2;
                queue_set = realloc(queue_set, queue_maxsize * sizeof (int*));
                queue_set_itemssize = realloc(queue_set_itemssize, queue_maxsize * sizeof (size_t));
            }
            tree_add_from(deja_vu, addset, addsetsize); // on ajoute pour plus le revoir dans la queue.
        }
        free(set);
    }
    tree_deep_delete(deja_vu);
    free(deja_vu);
    
    free(queue_set_itemssize);
    free(queue_set);
    
    //realloc
    ret.trans = realloc(ret.trans, ret.size* sizeof(liste**));
    ret.labels = realloc(ret.labels, ret.size* sizeof(char*));
    ret.initial = realloc(ret.initial, ret.size* sizeof(bool));
    ret.final = realloc(ret.final, ret.size* sizeof(bool));

    return ret;
}


/**
* Construit un automate d'exemple
* @return l'automate
*/
automate construitAutomateTest1() {
   automate ret = {.size = 3, .sizealpha = 3, .error = false};
   ret.initial = calloc(ret.size, sizeof (bool));
   ret.initial[0] = true;

   ret.final = calloc(ret.size, sizeof (bool));
   ret.final[2] = true;

   ret.trans = malloc(ret.size * sizeof (liste**));

   for (int i = 0; i < ret.size; ++i) {
       ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
   }

   ret.labels = malloc(ret.size * sizeof (liste**));
   for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
       ret.labels[i] = calloc(calloc_len, sizeof (char));
       sprintf(ret.labels[i], "%i", i);
   }


   ret.error |= !ajouteTransition(ret, "0", "1", 'c');
   ret.error |= !ajouteTransition(ret, "1", "1", 'a');
   ret.error |= !ajouteTransition(ret, "1", "2", 'a');
   ret.error |= !ajouteTransition(ret, "2", "1", 'b');
   return ret;
}


/**
* Construit un automate d'exemple
* @return l'automate
*/
automate construitAutomateTest2() {
   automate ret = {.size = 4, .sizealpha = 2, .error = false};
   ret.initial = calloc(ret.size, sizeof (bool));
   ret.initial[0] = true;

   ret.final = calloc(ret.size, sizeof (bool));
   ret.final[3] = true;

   ret.trans = malloc(ret.size * sizeof (liste**));

   for (int i = 0; i < ret.size; ++i) {
       ret.trans[i] = calloc(ret.sizealpha, sizeof (liste*));
   }

   ret.labels = malloc(ret.size * sizeof (liste**));
   for (int i = 0, calloc_len = floor(log10(abs(ret.size))) + 2; i < ret.size; ++i) {
       ret.labels[i] = calloc(calloc_len, sizeof (char));
       sprintf(ret.labels[i], "%i", i);
   }


   ret.error |= !ajouteTransition(ret, "0", "3", 'a');
   ret.error |= !ajouteTransition(ret, "0", "1", 'b');
   ret.error |= !ajouteTransition(ret, "1", "3", 'a');
   ret.error |= !ajouteTransition(ret, "1", "2", 'b');
   ret.error |= !ajouteTransition(ret, "2", "3", 'a');
   ret.error |= !ajouteTransition(ret, "2", "3", 'b');
   return ret;
}



/*
 * 
 */
int main(int argc, char** argv) {
    /*automate a = construitAutomateExemple();
    if (a.error) {
        perror("Erreur lors de l'initialisation.");
        return (EXIT_FAILURE);
    }
    afficheAutomate(a);
    printf("Nombre Transition : %i \n", compteTransitions(a));
    printf("Deterministe : %s \n", deterministe(a) ? "true" : "false");
    printf("Complet : %s \n", complet(a) ? "true" : "false");
    //supprimeEtat(&a, "3");
    //completeAutomate(a);
    //afficheAutomate(a);
    //fusionEtats(&a, "0", "1");
    afficheAutomate(a);
    //printf("%i\n", chemin(tmp, a.size, "0", "3"));
    supprimeEtat(&a, "2");
    afficheAutomate(a);
    supprimeEtat(&a, "3");
    afficheAutomate(a);
    supprimeNonAccessibles(&a);
    //afficheAutomate(a);
    automate _7_5_1 = construction_automate_7_5_1();
    afficheAutomate(_7_5_1);
    automate _7_5_2 = construction_automate_7_5_2();
    afficheAutomate(_7_5_2);
    automate prod = produit(_7_5_1, _7_5_2);
    afficheAutomate(prod);
    printf("%s\n", prod.labels[1 * 3 + 2]);
    tree_t *tmp = tree_create_root(false);
    fflush(stdout);
    int t1[4] = {1, 2, 4, 7};
    tree_add_from(tmp, t1, 4);
    int t2[5] = {1, 3, 4, 6, 10};
    tree_add_from(tmp, t2, 5);
    printf("%i\n", tree_est_reconus(tmp, t2, 5));
    fflush(stdout);
    //printf("%i\n", tmp->childs[0]->childs[0]->value);
    automate _7_6 = construction_automate_7_6();
    automate det = determinise(_7_6);
    printf("nombr : %i\n", det.size);
    afficheAutomate(det);
    fflush(stdout);*/
    
    
    automate a1 = construitAutomateTest2();
    automate a1det = determinise(a1);
    delAutomate(a1);
    afficheAutomate(a1det);
    
    return (EXIT_SUCCESS);
}

